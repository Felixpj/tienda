package com.tienda.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tienda.entity.Clientes;
import com.tienda.repository.ClientesRepository;

@Service
public class ClientesServiceImpl implements ClientesService{

	@Autowired
	ClientesRepository repository;
	
	@Override
	public List<Clientes> findAllClientes() {
		return repository.findAll();
	}

}
