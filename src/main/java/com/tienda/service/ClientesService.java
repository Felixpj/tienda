package com.tienda.service;

import java.util.List;

import com.tienda.entity.Clientes;


public interface ClientesService {

	public List<Clientes> findAllClientes();
	
}
