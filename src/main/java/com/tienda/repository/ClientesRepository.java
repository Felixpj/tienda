package com.tienda.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tienda.entity.Clientes;

@Repository
public interface ClientesRepository extends JpaRepository<Clientes,Long>{

}
