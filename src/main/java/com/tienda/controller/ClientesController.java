package com.tienda.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tienda.entity.Clientes;
import com.tienda.service.ClientesService;

@RequestMapping(value = "/clientes")
@RestController
public class ClientesController {
	
	@Autowired
	ClientesService clientesService;
	
	@RequestMapping(value = "/prueba")
	   public String hello() {
	      return "Probando controlador Clientes";
	   }
	
	//al llamar al controlador obtenemos el mensaje sin acceder a ninguna otra capa del servicio
	@RequestMapping(value= "/texto" , method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<String> getClientes() {
			return new ResponseEntity<>("Lista de Clientes", HttpStatus.OK);
	}
	
	//al llamar al controlador obtenemos un listado de los clientes que tenemos en la base de datos
	@RequestMapping(value= "/listaClientes" , method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<List<Clientes>> getListaClientes() {
			return new ResponseEntity<List<Clientes>>(clientesService.findAllClientes(), HttpStatus.OK);
	}
	
}
